package controlador.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import controlador.entity.Cliente;

@Repository//reconoce como bean
public class ClienteDAOclase implements ClienteDAO{

	@Override
	@Transactional//EVITA USAR BEGIN,COMMIT O ROLLBACK
	public List<Cliente> getClientes() {
		//Obtener la Session
		Session miSession=sessionFactory.getCurrentSession();
		
		//Crear la consulta(Query)
		Query<Cliente> miQuery=miSession.createQuery("from Cliente", Cliente.class);
		
		//Ejecutar Query y devolver resultados
		List<Cliente> clientes =miQuery.getResultList();
		return clientes;
	}

	
	@Autowired
	private SessionFactory sessionFactory;

	
	@Override
	@Transactional//EVITA USAR BEGIN,COMMIT O ROLLBACK
	public void insertarCliente(Cliente elCliente) {
		//Obtener la Session
		Session miSession=sessionFactory.getCurrentSession();

		//insertar el cliente
		//miSession.save(elCliente);
		miSession.saveOrUpdate(elCliente);//guarda o actualiza
	}

	
	@Override
	@Transactional//EVITA USAR BEGIN,COMMIT O ROLLBACK
	public Cliente getCliente(int id) {
		//Obtener la Session
		Session miSession=sessionFactory.getCurrentSession();
		
		//Obtener la info del cliente seleccionado
		Cliente elCliente=miSession.get(Cliente.class, id);
		return elCliente;
	}


	@Override
	@Transactional
	public void eliminarCliente(int id) {
		//Obtener la Session
		Session miSession=sessionFactory.getCurrentSession();		
		
		//Borro el cliente usando el Id del cliente
		Query consulta=miSession.createQuery("delete from Cliente where id=:IdCliente");
		consulta.setParameter("IdCliente", id);
		consulta.executeUpdate(); 
	}
	
	
	
}
