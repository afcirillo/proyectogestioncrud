package controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import controlador.dao.ClienteDAO;
import controlador.entity.Cliente;

@Controller
@RequestMapping("/cliente")
public class Controlador {
	
	
	@RequestMapping("/paginaPrincipal")	
	public String muestraPagina() {
		return "paginaPrincipal";
	}
	
	
	@RequestMapping("/lista")
	public String listaClientes(Model elModelo) {
		
		//obtener los clientes desde el DAO
		List<Cliente> losClientes=clienteDAO.getClientes();
		
		//agregar los clientes al modelo
		elModelo.addAttribute("clientes",losClientes);
		return "lista-clientes";
	}

	
	@RequestMapping("/muestraFormularioAgregar")
	public String muestraFormularioAgregar(Model elModelo) {
		
		//Bin de datos de clientes
		Cliente elCliente=new Cliente();
		elModelo.addAttribute("cliente", elCliente);
		
		return "formularioCliente";
	}
	
	
	@PostMapping("/insertarCliente")
	public String insertarCliente(@ModelAttribute("cliente")Cliente elCliente) {
		
		//insertar cliente en BBDD
		clienteDAO.insertarCliente(elCliente);
		
		return "redirect:/cliente/lista";
	}
	
	
	@GetMapping("/muestraFormularioActualizar")
	public String muestraFormularioActualizar(@RequestParam("clienteId") int Id, Model elModelo) {
		
		//Obtener el cliente cuyo id le pasamos por parametro
		Cliente elCliente=clienteDAO.getCliente(Id);
		
		//Establecer el cliente como atributo del modelo
		elModelo.addAttribute("cliente", elCliente);
		
		//Enviar al formulario
		return "formularioCliente";
	}
	
	
	@GetMapping("/eliminar")
	public String eliminarCliente(@RequestParam("clienteId") int Id) {
		
		//elimino el cliente
		clienteDAO.eliminarCliente(Id);
		
		//Redirect al formulario
		return "redirect:/cliente/lista";
	}
	
	
	@Autowired
	private ClienteDAO clienteDAO;
	
	
	
}
