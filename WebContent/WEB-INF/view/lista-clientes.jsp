<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    <!--importar el tag -->
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista de Clientes</title>

<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css"/>

</head>
<body>

<h1>

Lista de Clientes

</h1>

<table border="1">

	<tr>
	
		<th>Nombre</th>
		<th>Apellido</th>
		<th>Email</th>
		
		<th>Modificar</th>
		<th>Eliminar</th>
	
	
	</tr>
	
	
	<c:forEach var="clienteTemp" items="${clientes }">
	
		<c:url  var="linkActualizar" value="/cliente/muestraFormularioActualizar"> <!-- linkActualizar me lleva a esta url -->
		
			<c:param name="clienteId" value="${clienteTemp.id }"/>	<!-- especifica en  la url el id del cliente a moifica -->
		
		</c:url>
		
		<c:url  var="linkEliminar" value="/cliente/eliminar"> <!-- linkEliminar me lleva a esta url -->
		
			<c:param name="clienteId" value="${clienteTemp.id }"/>	<!-- especifica en  la url el id del cliente a moifica -->
		
		</c:url>
	
		<tr>
			<td>${clienteTemp.nombre}</td>
			<td>${clienteTemp.apellido}</td>
			<td>${clienteTemp.email}</td>
			
			<td><a href="${linkActualizar }"><input type="button" value="Modificar"/></a></td>
			<td><a href="${linkEliminar }"><input type="button" value="Eliminar" 
			onclick="if(!confirm('�Est�s seguro de que quieres eliminar el registro?')) return false"/></a></td>
			
		</tr>
		
		
	</c:forEach>
	
	
	
</table>

<br/>


<div class="botonAgregar">
 <a href="muestraFormularioAgregar"><button type="submit">Agregar Cliente</button></a>
</div>


</body>
</html>